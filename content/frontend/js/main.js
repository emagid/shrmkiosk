$(document).ready(function(){


  // opening animations
  $('.background').css('background-color', 'transparent');

  setTimeout(function(){
    $('.intro_text#home_intro').fadeIn(1000);
  }, 300);

  setTimeout(function(){
    $('.intro_text#home_intro').fadeOut(1000);
    $('.choices').fadeIn(1500);
      $('.sponsor_logos').fadeIn(1500);
    $('.choices').css('margin-top', '0');
  }, 3000);

  setTimeout(function(){
    $('footer').slideDown();
    $('footer').css('display', 'flex');
  }, 4500)



    // no rights clicks
    document.addEventListener('contextmenu', event => event.preventDefault());


   


     // initial timeout redirect homepage
        var initial = null;

        function invoke() {
            initial = window.setTimeout(
                function() {
                    window.location.href = '/';
                }, 60000);
        }

        invoke();

        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            invoke();
        });
    
    
        // no rights clicks
    document.addEventListener('contextmenu', event => event.preventDefault());


    // options animation 
    setTimeout(function(){
        $('.himms').fadeOut();
        $('.options').css('opacity', '1');
        $('.options').css('margin-top', '150px');
    }, 2000);

        $('.himms').fadeIn();
        $('.himms').css('margin-top', '-400px');

    $('.trivia_content').fadeIn('fast');
    $('.trivia_content').css('padding-top', '0px');


    if ( $('.home').hasClass('photobooth') ) {
        $('#video').fadeIn();
        $('#video').fadeIn();
        $('.pic_text').fadeIn(2000);
        $('#snap_photo').fadeIn(2000);
        $('#snap_gif').fadeIn(2000);
    };


    // / Choice click
    var timer;
    $(".click_action").on({
         'click': function clickAction() {
             var self = this;
                $(self).css('transform', 'scale(.8)');
              timer = setTimeout(function () {
                  $(self).css('transform', 'scale(1)');
              }, 100);
         }
    });


    // Choice click delay
    $('.option').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
            $('.option').css('opacity', '0'); 
            $(this).css('opacity', '1'); 
            $(this).css('transform', 'scale(1.4)'); 
            $('.himms').fadeOut();
        setTimeout(function(){
            if ( $(this).not('#choose_picture') ) {
                window.location.href = link;
            }
        }, 1000);
    });


});



